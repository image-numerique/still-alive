import processing.pdf.*;
import processing.sound.*;

SoundFile monsuperson;
Amplitude amp;
PFont SavateFont;

void setup(){
size(421,596);

monsuperson = new SoundFile(this, "cri_mort.mp3");
SavateFont = createFont("savate-regular.otf", 80);

amp = new Amplitude(this);
amp.input(monsuperson);

beginRecord(PDF, "still_alive.pdf");
background(#D6FFAE);
textFont(SavateFont);


}


void draw(){
 
  float fort = amp.analyze()*2000;
 //  ellipse(x,y,width,height);
 noFill();
  ellipse(random(fort/2),fort/2,fort/10,fort/10);


 println(fort);
}

void keyPressed() {
  
    if (key == 'a') {
background(#D6FFAE);
    monsuperson.play();
  
    }
    
        if (key == 'b') {
 fill(255,0,0);
 textFont(SavateFont,random(20,80));
text("Arrrrrrggggg",random(20),random(80,400));

textFont(SavateFont,30);
text("Stay Alive Poster",20,500);

textFont(SavateFont,15);
text("le "+day()+" - "+month()+" - "+year()+" à "+hour()+"h"+minute(),20,550);
    }
  
  
  
  if (key == 'c') {
    endRecord();
 //   exit();
  }
  
  
}
