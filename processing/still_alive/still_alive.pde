import processing.sound.*;
SoundFile monsuperson;
Amplitude amp;
PFont SavateFont;

void setup(){
size(421,596);
background(#D6FFAE);
//ATTENTION, il y a souvent des probs de lecture de vos .mp3
//Le mieux est de passer votre .mp3 par Miro Vidéo Converter
monsuperson = new SoundFile(this, "cri_mort.mp3");
SavateFont = createFont("savate-regular.otf", 80);
 monsuperson.play();
amp = new Amplitude(this);
amp.input(monsuperson);


textFont(SavateFont);
fill(255,0,0);
text("Arrrrrrggggg",20,80);

}


void draw(){
  
  float fort = amp.analyze()*2000;
 //  ellipse(x,y,width,height);
 noFill();
  ellipse(random(fort/2),fort/2,fort/10,fort/10);

 println(fort);

}
