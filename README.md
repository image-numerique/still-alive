## STILL ALIVE

# Introduction

L'idée est de pousser un cri humain afin d'affirmer qu'on est encore bien vivant malgré cette atmosphère assez morbide et bloquante. 

Elle est doublée d'une volonté de rendre hommage à la véritable voix humaine remplie de personnalité et de nuances à l'inverse de celles qui remplissent de plus en plus nos vies avec l'intelligence artificielle et les voix synthétiques (Amazon Alexia, Google Assistant, voix GPS, ...)

Nous pouvons partir du projet "Repeat after me" du studio Moniker concernant cet hommage :
https://studiomoniker.com/projects/repeat-after-me

# Méthode

Chaque étudiant va donc enregistrer son cri du cœur, son murmure de désespoir ou de jouissance et l'intégrer dans un processing pour créer un poster avec des formes géométriques simples. C'est l'analyse de l'amplitude de ce cri qui influencera la création.

# Son

Vos .mp3 créés sur smartphone ou autres ne sont pas toujours lisibles sur Processing. Le plus simple est de le passer à la moulinette "Miro Video Converter"

http://www.mirovideoconverter.com/


# Collaboration

Il serait intéressant d'afficher tous ces posters dans l'école et de le faire en sérigraphie au cours d'outils en master 1 (Dallemagne, Renzoni). Genre A4, 1 couleur sur papier de couleur - tiré 10X chaque. 

# Installation

Ces posters feront partie d'une installation qui serait amplifiée avec les cris enregistrés.

